module Main where

import Sokoban ( game )
import Env ( copyEnvPortWithDefault )

main :: IO ()
main = do
    copyEnvPortWithDefault "PORT" "CODEWORLD_API_PORT" 3030
    game
