{-# LANGUAGE OverloadedStrings #-}

module Picture
    ( drawWorldState
    , startScreen
    , finishScreen
    , levelFinishedScreen
    , reachabilityPicture
    , isClosedAndSanePicture
    ) where

import CodeWorld
import qualified Data.Text as T
import Util
    ( coloredPair, merge, roundRotates, scaledSame, translatedSame )
import World ( WorldState(..) )
import Maze
    ( Tile(Storage, Ground, Wall, Box)
    , Position(..)
    , Maze(..)
    , Map(..)
    , PlayerDirection(LeftDirection, RightDirection, UpDirection, DownDirection)
    , isLeftRight
    , reachableInMaze
    , isClosed
    , isSane
    )

boxBaseColor :: Color
boxBaseColor = HSL (pi / 12) 0.75 0.2

wallBaseColor :: Color
wallBaseColor = HSL (pi / 36) 0.85 0.35

wallBackgroundColor :: Color
wallBackgroundColor = HSL (pi / 36) 0 0.4

extraBackgroundColor :: Color
extraBackgroundColor = HSL (pi / 36) 0 0.75

groundColor :: Color
groundColor = HSL (pi / 36) 0 0.55

crossColor :: Color
crossColor = HSL (pi / 4) 1 0.5

playerBottomColor :: Color
playerBottomColor = HSL (pi / 8) 1 0.15

faceColor :: Color
faceColor = HSL (23 * pi / 12) 0.95 0.8

bodyColor :: Color
bodyColor = HSL (5 * pi / 4) 0.75 0.5

hatColor :: Color
hatColor = HSL (pi / 4) 1 0.6

menuColor :: Color
menuColor = HSL 0 0 0.2

trueColor :: Color
trueColor = HSL (2 * pi / 3) 1 0.5

falseColor :: Color
falseColor = HSL (2 * pi) 1 0.5

baseBlockCorner :: Double
baseBlockCorner = 0.35

baseBlockArcThick :: Double
baseBlockArcThick = 0.1

baseBlock :: Picture
baseBlock = solidRectangle 1 1

squaredBaseBlock :: Picture
squaredBaseBlock = solidPolygon points
  where rotatedPoints basePoint = [rotatedPoint (i * pi / 2) basePoint | i <- [1..4]]
        halfArcThick = 0.5 * baseBlockArcThick
        points1 = rotatedPoints (0.5, baseBlockCorner - halfArcThick)
        points2 = rotatedPoints (baseBlockCorner - halfArcThick, 0.5)
        points = merge [points1, points2]

roundedBaseBlock :: Picture
roundedBaseBlock = pictures $ base:arcs
  where baseBlockCorner = 0.35
        baseBlockArcThick = 0.1
        halfArcThick = 0.5 * baseBlockArcThick
        base = squaredBaseBlock
        arcTranslate = baseBlockCorner - halfArcThick
        arc = translatedSame arcTranslate $ thickArc baseBlockArcThick 0 (pi / 2) (0.5 - baseBlockCorner)
        arcs = [rotated (i * pi / 2) arc | i <- [1..4]]

boxBorder :: Double -> Picture
boxBorder borderWidth = pictures borders
  where part = scaled 1 borderWidth baseBlock
        moved = translated 0 (0.5 * (1 - borderWidth)) part
        borders = [rotated (pi / 2 * i) moved | i <- [0..3]]

crossOfWidth :: Double -> Picture
crossOfWidth borderWidth = pictures [cross1, cross2]
  where part = scaled ((1 - 2 * borderWidth) * sqrt 2) borderWidth baseBlock
        cross1 = rotated (pi / 4) part
        cross2 = rotated (- pi / 4) part

coloredBoxInside :: Color -> Color -> Double -> Picture
coloredBoxInside c1 c2 borderWidth = pictures coloredBlocks
    where part = scaled borderWidth 1 baseBlock
          colors = cycle [c1, c2]
          partsCount = 1.0 / borderWidth
          blocks = [translated (i * borderWidth - 0.5 * (1 + borderWidth)) 0 part | i <- [1..partsCount]]
          coloredBlocks = zipWith (curry coloredPair) colors blocks

box :: Bool -> Picture
box onStorage = pictures [border, cross, inside, background]
  where borderWidth = 0.1
        baseColor = if onStorage then crossColor else boxBaseColor
        border = colored (darker 0.1 baseColor) $ boxBorder borderWidth
        cross = colored (lighter 0.08 baseColor) $ crossOfWidth borderWidth
        inside =  coloredBoxInside (lighter 0.05 baseColor) (darker 0.05 baseColor) borderWidth
        background = colored baseColor baseBlock

wall :: Picture
wall = pictures $ reverse $ background:quarters
  where inScale = 0.4 -- < 0.5
        inMoveCorner = 0.25
        base = colored wallBaseColor squaredBaseBlock
        quarter (x, y) = translated (x * inMoveCorner) (y * inMoveCorner) 
                       $ scaled inScale inScale base
        extra = quarter (0.0, -1.0)
        quarters = extra:map quarter roundRotates
        background = colored wallBackgroundColor baseBlock

ground :: Picture
ground = pictures [inside, background]
  where inside = colored groundColor
               $ scaledSame 0.95 squaredBaseBlock
        background = colored (darker 0.2 groundColor) baseBlock

storage :: Picture
storage = pictures [cross, ground]
  where cross = colored crossColor 
              $ scaledSame 0.8
              $ crossOfWidth 0.3

extraBackground :: Picture
extraBackground = colored extraBackgroundColor baseBlock

playerBottom :: Picture
playerBottom = colored playerBottomColor bottom
   where leg = translated 0 (-0.35) $ scaled 0.4 0.3 roundedBaseBlock
         leftLeg = translated (-0.3) 0 leg
         rightLeg = translated 0.3 0 leg
         top = translated 0 (-0.15) $ scaled 1 0.4 baseBlock
         bottom = translated 0 0.05
                $ scaled 0.32 1
                $ pictures [top, leftLeg, rightLeg]
         
playerTop :: PlayerDirection ->Picture
playerTop direction = hands & body
   where body = colored bodyColor
              $ translated 0 (-0.05) 
              $ scaledSame 0.36 roundedBaseBlock
         hand = translated 0 (-0.1)
              $ colored (darker 0.25 bodyColor)
              $ scaled 0.2 0.8 body
         angle = pi / 6
         right = rotated angle hand
         left = rotated (-angle) hand  
         xH = 0.17
         uxH = 1.8 * xH
         yH = 0.16                                      
         hands = case direction of LeftDirection -> left
                                   RightDirection -> right
                                   UpDirection -> translated (-uxH)  yH right
                                                & translated uxH yH left
                                   DownDirection -> translated (-xH) 0 left
                                                  & translated xH 0 right

playerHead :: PlayerDirection -> Picture
playerHead direction = translated 0 0.2 $ pictures [hat, face]
  where face = colored faceColor $ scaledSame 0.4 roundedBaseBlock
        hatSize = 0.26
        halfSize = hatSize / 2
        trans = halfSize * case direction of LeftDirection -> -1.0
                                             RightDirection -> 1.0
                                             _ -> 0.0
        hatPart = if isLeftRight direction 
                  then translated trans halfSize
                     $ scaled hatSize hatSize roundedBaseBlock
                  else blank
        hat = colored hatColor $ sector 0 pi hatSize & hatPart

player :: PlayerDirection -> Picture
player direction = playerHead direction 
                 & playerTop direction 
                 & playerBottom

textBackground :: Double -> Picture
textBackground width = background
  where color = colored menuColor
        backgroundPart = color $ scaledSame (width / 2) roundedBaseBlock
        background = translated 0 0.25
                   $ color $ scaled width (width / 2) baseBlock
                   & translated (width / 2) 0 backgroundPart
                   & translated (-width / 2) 0 backgroundPart

finishScreen :: Picture
finishScreen = edited & textBackground 7
  where winText = styledLettering Bold Monospace "You Win"
        edited = colored white $ scaledSame 2 winText

startScreen :: Picture
startScreen = edited & textBackground 7
  where helloText = styledLettering Bold Monospace "Sokoban"
        edited = colored white $ scaledSame 2 helloText

levelFinishedScreen :: Int -> Picture
levelFinishedScreen count = edited & textBackground 9
  where helloText = styledLettering Bold Monospace $ T.pack 
                  $ "Finished in " ++ show count ++ " moves"
        edited = colored white helloText

drawTile :: Tile -> Picture
drawTile tile = case tile of Wall -> wall
                             Ground -> ground
                             Storage -> storage
                             Box -> box False
                             _ -> extraBackground

drawPictureAt :: Position -> Picture -> Picture
drawPictureAt (Position x y) = translated (fromIntegral x) (fromIntegral y)

drawTileAt :: Position -> Tile ->  Picture
drawTileAt p = drawPictureAt p . drawTile

drawPlayerAt :: PlayerDirection ->  Position -> Picture
drawPlayerAt d p = drawPictureAt p $ player d

drawMaze :: WorldState -> Picture
drawMaze world = pictures $ map (\p -> drawTileAt p $ worldMap p) mazePoints
  where maze = worldMaze world
        range = mapRange . mazeMap $ maze
        worldMap = mapTile . mazeMap $ maze
        mazePoints = [Position x y | x <- range, y <- range]

drawBoxes :: WorldState -> Picture
drawBoxes world = pictures 
                $ map (\(p, b) -> drawPictureAt p $ box b) placedPositions
  where positions = worldBoxes world
        worldMap = mapTile . mazeMap . worldMaze $ world
        boxOnStorage p = Storage == worldMap p
        placedPositions = map (\p -> (p, boxOnStorage p)) positions

drawWorldState :: WorldState -> Picture
drawWorldState world = player & boxes & mazePicture
  where maze = worldMaze world
        position = playerPosition maze
        direction = playerDirection maze
        player = drawPlayerAt direction position
        boxes = drawBoxes world
        mazePicture = drawMaze world

pictureOfBoolAt :: (Bool, Position) -> Picture
pictureOfBoolAt (b, p) = drawPictureAt p $ scaledSame 0.95 $ pictureOfBool b
 where pictureOfBool True = colored trueColor baseBlock
       pictureOfBool False = colored falseColor baseBlock

reachabilityPicture :: Maze -> Picture
reachabilityPicture maze = pictures
                         $ zipWith (curry pictureOfBoolAt) reach mapPos
  where m = mazeMap maze
        range = mapRange m
        mapPos = [Position x y | x <- range, y <- range]
        reach = reachableInMaze maze

isClosedAndSanePicture :: [Maze] -> Picture
isClosedAndSanePicture mazes = editedIsClosedText & drawBools isClosed 1 
                             & editedIsSaneText & drawBools isSane (-3)
  where isClosedText = styledLettering Bold Monospace "Is closed:"
        editedIsClosedText = drawPictureAt (Position 0 3) isClosedText
        isSaneText = styledLettering Bold Monospace "Is sane:"
        editedIsSaneText = drawPictureAt (Position 0 (-1)) isSaneText
        len = length mazes `div` 2
        drawBools predicate y = pictures
                              $ zipWith (curry pictureOfBoolAt) 
                                  (map predicate mazes)
                                  [Position x y | x <- [-len..len + 1]]