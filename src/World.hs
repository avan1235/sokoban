module World
    ( movePlayer
    , isWinning
    , createWorldState
    , nextLevel
    , Move(..)
    , WorldState(..)
    , Levelable
    ) where

import Maze ( extractBoxes
            , removeBoxesFromMaze
            , reachabilityPositionsInMaze
            , Tile(..)
            , Maze(..)
            , PlayerDirection(..)
            , Map(..)
            , Position(..) )

data Move = MoveUp | MoveDown | MoveLeft | MoveRight

class Levelable a where
  nextLevel :: a -> Maybe a
  isWinning :: a -> Bool

data WorldState = WorldState {
  worldBoxes :: [Position],
  worldMaze :: Maze,
  nextMazes :: [Maze]
} deriving Eq

createWorldState :: [Maze] -> WorldState
createWorldState mazes = WorldState {
  worldMaze = removeBoxesFromMaze maze,
  nextMazes = tail mazes,
  worldBoxes = extractBoxes maze
}
  where maze = head mazes

instance Levelable WorldState where
  
  nextLevel world = modifiedWorld next
    where next = nextMazes world
          modifiedWorld [] = Nothing
          modifiedWorld mazes = Just $ createWorldState mazes

  isWinning world = and onPlaces
    where maze = worldMaze world
          worldMap = mapTile . mazeMap $ maze
          isStorage p = Storage == worldMap p
          reachable = map fst 
                    $ filter snd
                    $ reachabilityPositionsInMaze maze
          isReachable b = b `elem` reachable
          boxes = filter isReachable 
                $ worldBoxes world
          onPlaces = map isStorage boxes

movePointBy :: Position -> Move -> Position
movePointBy (Position x y) m = case m of MoveUp -> Position x (y + 1)
                                         MoveDown -> Position  x (y - 1)
                                         MoveLeft -> Position (x - 1) y
                                         MoveRight -> Position (x + 1) y
                                
canPlayerMoveTo :: Map -> Position -> Move -> [Position] -> Bool
canPlayerMoveTo map position byMove boxes = canBeThere position || canMoveBox position
  where empty = mapTile map
        canBeThere p = (p `notElem` boxes) && (Ground == empty p || Storage == empty p)
        canMoveBox p = (p `elem` boxes) && canBeThere newBox
          where newBox = movePointBy p byMove

updatedDirection :: Move -> PlayerDirection -> PlayerDirection
updatedDirection MoveLeft _ = LeftDirection
updatedDirection MoveRight _ = RightDirection
updatedDirection MoveUp _ = UpDirection
updatedDirection MoveDown _ = DownDirection

updatedPosition :: Map -> Position -> Position -> Move -> [Position] -> Position
updatedPosition map new old move boxes = 
  if canPlayerMoveTo map new move boxes then new else old

movePlayer :: Move -> WorldState -> WorldState
movePlayer move world = world {
                          worldBoxes = updatedWorldBoxes,
                          worldMaze = maze {
                            playerDirection = updatedPlayerDirection,
                            playerPosition = updatedPlayerPosition
                          }
                        }
  where maze = worldMaze world
        worldMap = mazeMap maze
        position = playerPosition maze
        movedPosition = movePointBy position move
        boxes = worldBoxes world
        updatedPlayerPosition = updatedPosition worldMap movedPosition position move boxes
        updatedPlayerDirection = updatedDirection move $ playerDirection maze
        notMovedBox = updatedPlayerPosition `notElem` boxes
        moveBox p = if p /= updatedPlayerPosition then p else movePointBy p move
        updatedWorldBoxes = if notMovedBox then boxes else map moveBox boxes
