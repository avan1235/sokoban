module Polymorphic
    ( foldListLeft
    , foldListRight
    , elemList
    , appendList
    , listLength
    , filterList
    , nth
    , mapList
    , andList
    , allList
    , isGraphClosed
    , reachable
    , allReachable
    , dfs
    ) where

foldListLeft :: (a -> b -> b) -> b -> [a] -> b
foldListLeft _ acc [] = acc
foldListLeft f acc (xh:xt) = foldListLeft f (f xh acc) xt

foldListRight :: (a -> b -> b) -> b -> [a] -> b
foldListRight _ acc [] = acc
foldListRight f acc (xh:xt) = f xh (foldListRight f acc xt)

elemList :: Eq a => a -> [a] -> Bool
elemList elem = foldListLeft (\e acc -> acc || e == elem) False

appendList :: [a] -> [a] -> [a]
appendList l1 l2 = foldListRight (:) l2 l1

listLength :: [a] -> Int
listLength = foldListLeft (\_ acc -> acc + 1) 0

filterList :: (a -> Bool) -> [a] -> [a]
filterList predicate = foldListLeft (\e acc -> if predicate e then e:acc else acc) []

nth :: [a] -> Int -> Maybe a
nth list index = snd (foldListLeft (\e (i, m) -> 
                                   (i + 1, if i == index then Just e else m)) 
                               (0, Nothing) 
                               list
                     )

mapList :: (a -> b) -> [a] -> [b]
mapList mapper = foldListLeft (\e acc -> mapper e:acc) []

andList :: [Bool] -> Bool
andList = foldListLeft (&&) True

allList :: (a -> Bool) -> [a] -> Bool
allList predicate = foldListLeft (\e acc -> acc && predicate e) True

dfs :: Eq a => (a -> [a]) -> a -> [a] -> [a]
dfs neighbours v visited
  | v `elemList` visited = visited
  | otherwise = foldListLeft (dfs neighbours) modVisited neigh
      where neigh = neighbours v
            modVisited = v:visited

isGraphClosed :: Eq a => a -> (a -> [a]) -> (a -> Bool) -> Bool
isGraphClosed initial neighbours isOk = allList isOk (dfs neighbours initial [])

reachable :: Eq a => a -> a -> (a -> [a]) -> Bool
reachable v initial neighbours = v `elemList` dfs neighbours initial []

allReachable :: Eq a => [a] -> a -> (a -> [a]) -> Bool
allReachable vs initial neighbours = allList reachableInGraph vs
  where reachableInGraph v = reachable v initial neighbours
