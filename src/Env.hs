module Env
    ( copyEnvPortWithDefault
    ) where

import System.Environment ( lookupEnv, setEnv )
import Data.Maybe ( fromMaybe )
import Text.Read ( readMaybe )

readPortFromEnv :: String -> Int -> IO Int
readPortFromEnv envName defaultPort = do
    ms <- lookupEnv envName
    return $ fromMaybe defaultPort (ms >>= readMaybe)

copyEnvPortWithDefault :: String -> String -> Int -> IO ()
copyEnvPortWithDefault from to defaultValue = do
    port <- readPortFromEnv from defaultValue
    setEnv to $ show port