module Util
    ( coloredPair
    , merge
    , rotates
    , roundRotates
    , scaledSame
    , translatedSame
    ) where

import CodeWorld ( Color, Picture, colored, scaled, translated )
import Data.List ( transpose )

coloredPair :: (Color, Picture) -> Picture
coloredPair (c, p) = colored c p

merge :: [[a]] -> [a]
merge = concat . transpose

rotates :: [Double]
rotates = [0.0, pi / 2, pi, 3 * pi / 4]

roundRotates :: [(Double, Double)]
roundRotates = [(1.0, 1.0), (1.0, -1.0), (-1.0, -1.0), (-1.0, 1.0)]

scaledSame :: Double -> Picture -> Picture
scaledSame d = scaled d d

translatedSame :: Double -> Picture -> Picture
translatedSame d = translated d d