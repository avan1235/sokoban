module Maze
    ( mazes
    , badMazes
    , extractBoxes
    , removeBoxes
    , removeBoxesFromMaze
    , isLeftRight
    , reachableInMaze
    , reachabilityPositionsInMaze
    , isClosed
    , isSane
    , Position(..)
    , Tile(..)
    , Maze(..)
    , PlayerDirection(..)
    , Map(..)
    ) where

import Polymorphic ( isGraphClosed
                   , dfs )

data Position = Position Int Int deriving Eq
instance Semigroup Position where
  (<>) (Position x1 y1) (Position x2 y2) = Position (x1 + x2) (y1 + y2)
instance Monoid Position where
  mempty = Position 0 0

positionFromPair :: (Int, Int) -> Position
positionFromPair (x, y) = Position x y

data Tile = Wall | Ground | Storage | Box | Background deriving Eq
data PlayerDirection = LeftDirection | RightDirection | UpDirection | DownDirection deriving Eq

data Map = Map {
  mapTile :: Position -> Tile,
  mapRange :: [Int]
}

type GraphNode = (Position, Tile)
type MapGraph = GraphNode -> [GraphNode]

canBeReached :: (Position, Tile) -> Bool
canBeReached (_, Ground) = True
canBeReached (_, Storage) = True
canBeReached (_, _) = False

mapToGraph :: Map -> MapGraph
mapToGraph m = graph
  where graph (n, t) = if inRange x && inRange y && canMoveTo (n, t)
                       then filter canMoveTo 
                          $ map toNode 
                          $ neigh n
                       else []
          where Position x y = n
                tile = mapTile m
                canMoveTo (p, t) = canBeReached (p, t) || t == Background
                toNode p = (p, tile p)
                inRange v = v `elem` mapRange m
                neigh p = map ((p <>) . positionFromPair) [(0, 1), (1, 0), (0, -1), (-1, 0)]

reachabilityPositionsInMaze :: Maze -> [(Position, Bool)]           
reachabilityPositionsInMaze maze = [toReachablePos $ Position x y | x <- range, y <- range]
  where initPos = playerPosition maze
        m = mazeMap maze
        range = mapRange m
        neighbours = (mapToGraph . removeBoxes) m
        initNode = (initPos, mapTile m initPos)
        nodes = map fst
              $ dfs neighbours initNode []
        toReachablePos p = (p, p `elem` nodes)

reachableInMaze :: Maze -> [Bool]           
reachableInMaze maze = map snd $ reachabilityPositionsInMaze maze

allFieldsInOrder :: Map -> [Tile]
allFieldsInOrder m = [t $ Position x y | x <- r, y <- r]
  where r = mapRange m
        t = mapTile m

instance Eq Map where
  (==) m1 m2 = allFields1 == allFields2
    where allFields1 = allFieldsInOrder m1
          allFields2 = allFieldsInOrder m2

data Maze = Maze {
  playerPosition :: Position,
  playerDirection :: PlayerDirection,
  mazeMap :: Map
} deriving Eq


isClosed :: Maze -> Bool
isClosed maze = allNotBackground && canBeReached initNode
  where initPos = playerPosition maze
        m = mazeMap maze
        initNode = (initPos, mapTile m initPos)
        notBackground (_, Background) = False
        notBackground (_, _) = True
        neighbours = (mapToGraph . removeBoxes) m
        allNotBackground = isGraphClosed initNode neighbours notBackground


isSane :: Maze -> Bool
isSane maze = sLen >= bLen
  where initPos = playerPosition maze
        m = mazeMap maze
        neighbours = (mapToGraph . removeBoxes) m
        initNode = (initPos, mapTile m initPos)
        bLen = length $ extractBoxes maze
        sLen = length
             $ filter (\(_, t) -> t == Storage)
             $ dfs neighbours initNode []

isInSquareOfSize :: Int -> Int -> Int -> Bool
isInSquareOfSize size x y = ax <= size && ay <= size
  where ax = abs x
        ay = abs y

isLeftRight :: PlayerDirection -> Bool
isLeftRight LeftDirection = True
isLeftRight RightDirection = True
isLeftRight _ = False

extractBoxes :: Maze -> [Position]
extractBoxes maze = map fst
                     $ filter (\(_, t) -> t == Box)
                     $ map (\p -> (p, mapTile m p))
                     $ filter isReachable
                     $ [Position x y | x <- range, y <- range]
  where m = mazeMap maze
        range = mapRange m
        reachable = map fst 
                  $ filter snd
                  $ reachabilityPositionsInMaze maze
        isReachable b = b `elem` reachable

removeBoxes :: Map -> Map
removeBoxes oldMap = oldMap { mapTile = modifiedMap }
  where modifiedMap p = let tile = mapTile oldMap p in 
          if tile /= Box then tile else Ground

removeBoxesFromMaze :: Maze -> Maze
removeBoxesFromMaze maze = maze { mazeMap = noBoxes }
  where noBoxes = (removeBoxes . mazeMap) maze

addBoxes :: [Position] -> Map -> Map
addBoxes positions oldMap = oldMap { mapTile = modifiedMap}
  where modifiedMap p = if p `elem` positions
                        then Box
                        else mapTile oldMap p 

createMaze :: (Int, Int) -> PlayerDirection -> [Int] -> (Position -> Tile) -> Maze
createMaze initPos direction range map = Maze {
  playerPosition = positionFromPair initPos,
  playerDirection = direction,
  mazeMap = Map {
    mapTile = map,
    mapRange = range
  }
}

mazeBase :: Maze
mazeBase = createMaze (3, 3) LeftDirection [-4..4] mazeBlocks
  where mazeBlocks (Position x y)
          | abs x > 4  || abs y > 4  = Background
          | abs x == 4 || abs y == 4 = Wall
          | x ==  2 && y <= 0        = Wall
          | x ==  3 && y <= 0        = Storage
          | x >= -2 && y == 0        = Box
          | otherwise                = Ground

mazeEasy :: Maze
mazeEasy = createMaze (-3, 0) RightDirection [-4..4] mazeBlocks
  where mazeBlocks (Position x y)
          | abs x > 4  || abs y > 1  = Background
          | abs x == 4 || abs y == 1 = Wall
          | x == 3 && y == 0         = Storage
          | x == 2 && y == 0         = Box
          | otherwise                = Ground

mazeEasyBad :: Maze
mazeEasyBad = createMaze (-3, 0) RightDirection [-4..4] mazeBlocks
  where mazeBlocks (Position x y)
          | abs x > 4  || abs y > 1  = Background
          | abs x == 4 || abs y == 1 = Wall
          | x == 3 && y == 0         = Storage
          | x == 2 && y == 0         = Wall
          | x == 1 && y == 0         = Box
          | otherwise                = Ground

mazeRound :: Maze
mazeRound = createMaze (3, 0) UpDirection [-7..7] mazeBlocks
  where mazeBlocks (Position x y)
          | isInSquareOfSize 0 x y             = Wall
          | x == 3 && y <= -1 && y >= -3       = Storage
          | abs x == 2 && y == 0               = Box
          | x == 0 && y == 2                   = Box
          | isInSquareOfSize 1 x y             = Ground
          | x == 1 && y == -2                  = Ground
          | isInSquareOfSize 2 x y             = Wall
          | x == 2 && y == -3                  = Wall
          | isInSquareOfSize 3 x y             = Ground
          | isInSquareOfSize 1 (x + 4) (y - 4) = Ground
          | isInSquareOfSize 1 (x - 4) (y - 4) = Ground
          | isInSquareOfSize 2 (x + 4) (y - 4) = Wall
          | isInSquareOfSize 2 (x - 4) (y - 4) = Wall
          | isInSquareOfSize 4 x y             = Wall
          | otherwise                          = Background

mazeRoundBad :: Maze
mazeRoundBad = createMaze (3, 0) UpDirection [-7..7] mazeBlocks
  where mazeBlocks (Position x y)
          | x == 0 && y == 0                   = Storage
          | x == -3 && y == 0                  = Box
          | isInSquareOfSize 1 x y             = Ground
          | isInSquareOfSize 2 x y             = Wall
          | isInSquareOfSize 3 x y             = Ground
          | isInSquareOfSize 1 (x + 4) (y - 4) = Ground
          | isInSquareOfSize 1 (x - 4) (y - 4) = Ground
          | isInSquareOfSize 1 (x + 4) (y + 4) = Ground
          | isInSquareOfSize 1 (x - 4) (y + 4) = Ground
          | isInSquareOfSize 2 (x + 4) (y - 4) = Wall
          | isInSquareOfSize 2 (x - 4) (y - 4) = Wall
          | isInSquareOfSize 2 (x + 4) (y + 4) = Wall
          | isInSquareOfSize 2 (x - 4) (y + 4) = Wall
          | isInSquareOfSize 4 x y             = Wall
          | otherwise                          = Background

mazeOpenBad :: Maze
mazeOpenBad = createMaze (-3, 0) RightDirection [-5..5] mazeBlocks
  where mazeBlocks (Position x y)
          | abs x > 4  || abs y > 1  = Background
          | abs y == 1               = Wall
          | x == 3 && y == 0         = Storage
          | x == 2 && y == 0         = Box
          | otherwise                = Ground

mazes :: [Maze]
mazes = [mazeEasy, mazeBase, mazeRound]

badMazes :: [Maze]
badMazes = [mazeEasyBad, mazeRoundBad, mazeOpenBad]