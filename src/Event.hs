{-# LANGUAGE OverloadedStrings #-}

module Event
    ( eventHandler
    , withUndo
    , resettable
    , withLevels
    , runActivity
    , Activity(..)
    ) where

import World
    ( Move(MoveRight, MoveLeft, MoveDown, MoveUp)
    , WorldState
    , movePlayer
    , isWinning
    , nextLevel
    , Levelable )
import CodeWorld 
    ( activityOf,  Event(KeyPress, KeyRelease)
    , Picture )

type EventHandler = Event -> WorldState -> WorldState
data GameState world = StartScreen | Running world Int | Finished (Maybe world) Int | GameOver deriving Eq
data Activity world = Activity world (Event -> world -> world) (world -> Picture)
data WithUndo activity = WithUndo activity [activity]

withUndo :: Eq world => Activity world -> Activity (WithUndo world)
withUndo (Activity world handle draw) = Activity modState modHandle modDraw
  where modState = WithUndo world []
        modHandle (KeyRelease "U") (WithUndo s stack)
          = case stack of xh:xt -> WithUndo xh xt
                          [] -> WithUndo s []
        modHandle e (WithUndo s stack)
          | s' == s = WithUndo s stack
          | otherwise = WithUndo s' (s:stack)
          where s' = handle e s
        modDraw (WithUndo s _) = draw s

eventHandler :: EventHandler
eventHandler (KeyRelease "Up") = movePlayer MoveUp
eventHandler (KeyRelease "Down") = movePlayer MoveDown
eventHandler (KeyRelease "Left") = movePlayer MoveLeft
eventHandler (KeyRelease "Right") = movePlayer MoveRight
eventHandler _ = id

resettable :: Activity w -> Activity w
resettable (Activity initState handler draw) = Activity initState modifiedHandler draw
  where resetWorldState _ = initState
        modifiedHandler (KeyPress "Esc") = resetWorldState
        modifiedHandler (KeyRelease "Esc") = resetWorldState
        modifiedHandler event = handler event

withLevels :: Levelable w => Picture -> Picture -> (Int -> Picture) -> Activity w -> Activity (GameState w)
withLevels startScreen gameOverScreen levelResultScreen (Activity world handle draw) = Activity StartScreen modHandle modDraw
  where modDraw StartScreen = startScreen
        modDraw (Running world _) = draw world
        modDraw (Finished _ n) = levelResultScreen n
        modDraw GameOver = gameOverScreen

        handleMove world e n = if isWinning world
                               then Finished (nextLevel world) (n - 1) 
                               else Running (handle e world) n

        modHandle (KeyPress " ") StartScreen = Running world 0
        modHandle _ StartScreen = StartScreen

        modHandle _ GameOver = GameOver

        modHandle (KeyPress "N") (Finished (Just world) _) = Running world 0
        modHandle (KeyPress "N") (Finished Nothing _) = GameOver
        modHandle _ (Finished w n) = Finished w n

        modHandle (KeyRelease k) (Running world n) = handleMove world (KeyRelease k) (n + 1)
        modHandle e (Running world n) = handleMove world e n

runActivity :: Activity w -> IO ()
runActivity (Activity world handle draw) = activityOf world handle draw
