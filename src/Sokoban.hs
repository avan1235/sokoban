module Sokoban
    ( game
    ) where

import Picture ( startScreen
               , drawWorldState
               , finishScreen
               , levelFinishedScreen )
import Maze ( mazes )
import World ( createWorldState
             , WorldState )
import Event ( eventHandler
             , runActivity
             , withUndo
             , resettable
             , withLevels
             , runActivity
             , Activity(..) )

initialWorldState :: WorldState
initialWorldState = createWorldState mazes

game :: IO ()
game = runActivity
     . resettable
     . withUndo
     . withLevels startScreen finishScreen levelFinishedScreen
     $ Activity initialWorldState eventHandler drawWorldState